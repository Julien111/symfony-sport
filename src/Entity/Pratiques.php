<?php

namespace App\Entity;

use App\Repository\PratiquesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PratiquesRepository::class)
 */
class Pratiques
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="pratiquesofusers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sportifpratiques;

    /**
     * @ORM\ManyToOne(targetEntity=Sports::class, inversedBy="pratiqueSportive")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sportsData;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getSportifpratiques(): ?Users
    {
        return $this->sportifpratiques;
    }

    public function setSportifpratiques(?Users $sportifpratiques): self
    {
        $this->sportifpratiques = $sportifpratiques;

        return $this;
    }

    public function getSportsData(): ?Sports
    {
        return $this->sportsData;
    }

    public function setSportsData(?Sports $sportsData): self
    {
        $this->sportsData = $sportsData;

        return $this;
    }
}
