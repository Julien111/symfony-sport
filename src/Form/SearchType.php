<?php

namespace App\Form;

use App\Entity\Sports;
use App\Entity\Pratiques;
use App\Entity\Departements;
use App\Repository\SportsRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('niveau', ChoiceType::class, [
            'label' => 'Choisir votre niveau :',
            'choices'  => [
                    'Niveau ?' => '',
                    'Débutant' => 'Débutant',
                    'Confirmé' => 'Confirmé',
                    'Professionnel' => 'Professionnel',
                    'Supporter' => 'Supporter',
                ],
            'required' => false])   
            ->add('sportsData', EntityType::class, [                
                'class' => Sports::class,
                'label' => 'Votre sport : ',
                'query_builder' => function (SportsRepository $sportValide) {
                    return $sportValide->createQueryBuilder('sports')                        
                        ->andWhere('sports.is_verified = 1');
                },
                'placeholder' => 'Sport *',
                'required' => true,
            ])
            ->add('departements', EntityType::class, [
                'class' => Departements::class,
                'choice_label' => 'name',
                'placeholder' => 'Département *',
                'label' => 'Votre département',
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // 'data_class' => Pratiques::class,
        ]);
    }
}