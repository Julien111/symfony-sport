<?php

namespace App\Controller\Admin;

use App\Entity\Sports;
use App\Form\SportsType;
use App\Repository\SportsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/sports") 
 * Require ROLE_ADMIN for *every* controller method in this class.
 * @IsGranted("ROLE_ADMIN")
 * 
 */
class AdminSportsController extends AbstractController
{
    /**
     * @Route("/", name="admin_sports_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(SportsRepository $sportsRepository): Response
    {
        return $this->render('admin_sports/index.html.twig', [
            'sports' => $sportsRepository->findBy(['is_verified' => false]),
        ]);
    }
    
    /**
     * @Route("/{id}", name="admin_sports_show", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(Sports $sport): Response
    {
        return $this->render('admin_sports/show.html.twig', [
            'sport' => $sport,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_sports_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Sports $sport): Response
    {
        $form = $this->createForm(SportsType::class, $sport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_sports_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_sports/edit.html.twig', [
            'sport' => $sport,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sports_delete", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Sports $sport): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sport->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sport);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_sports_index', [], Response::HTTP_SEE_OTHER);
    }
}