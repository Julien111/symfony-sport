<?php

namespace App\Repository;

use App\Entity\SportAttente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SportAttente|null find($id, $lockMode = null, $lockVersion = null)
 * @method SportAttente|null findOneBy(array $criteria, array $orderBy = null)
 * @method SportAttente[]    findAll()
 * @method SportAttente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SportAttenteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SportAttente::class);
    }

    // /**
    //  * @return SportAttente[] Returns an array of SportAttente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SportAttente
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
