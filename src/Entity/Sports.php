<?php

namespace App\Entity;

use App\Repository\SportsRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SportsRepository::class)
 */
class Sports
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Le nom du sport est trop court, vous devez mettre un nom plus long.",
     *      maxMessage = "Votre nom de sport est trop long, vous devez raccourcir le nom."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     *  * @Assert\Length(
     *      min = 10,  
     *      minMessage = "Votre description doit être plus longue.",     
     * )
     */
    private $description;

    /** 
     * @ORM\OneToMany(targetEntity=Pratiques::class, mappedBy="sportsData", orphanRemoval=true)
     */
    private $pratiqueSportive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_verified;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="sportsCreate", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $userSportif;

    public function __toString() {

    return $this->name;
    
    }

    public function __construct()
    {
        $this->pratiqueSportive = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Pratiques[]
     */
    public function getPratiqueSportive(): Collection
    {
        return $this->pratiqueSportive;
    }

    public function addPratiqueSportive(Pratiques $pratiqueSportive): self
    {
        if (!$this->pratiqueSportive->contains($pratiqueSportive)) {
            $this->pratiqueSportive[] = $pratiqueSportive;
            $pratiqueSportive->setSportsData($this);
        }

        return $this;
    }

    public function removePratiqueSportive(Pratiques $pratiqueSportive): self
    {
        if ($this->pratiqueSportive->removeElement($pratiqueSportive)) {
            // set the owning side to null (unless already changed)
            if ($pratiqueSportive->getSportsData() === $this) {
                $pratiqueSportive->setSportsData(null);
            }
        }

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->is_verified;
    }

    public function setIsVerified(bool $is_verified): self
    {
        $this->is_verified = $is_verified;

        return $this;
    }

    public function getUserSportif(): ?Users
    {
        return $this->userSportif;
    }

    public function setUserSportif(?Users $userSportif): self
    {
        $this->userSportif = $userSportif;

        return $this;
    }    
}