<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211031214654 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratiques ADD sports_data_id INT NOT NULL');
        $this->addSql('ALTER TABLE pratiques ADD CONSTRAINT FK_81B08E0A11DACE4C FOREIGN KEY (sports_data_id) REFERENCES sports (id)');
        $this->addSql('CREATE INDEX IDX_81B08E0A11DACE4C ON pratiques (sports_data_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratiques DROP FOREIGN KEY FK_81B08E0A11DACE4C');
        $this->addSql('DROP INDEX IDX_81B08E0A11DACE4C ON pratiques');
        $this->addSql('ALTER TABLE pratiques DROP sports_data_id');
    }
}
