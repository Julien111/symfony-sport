<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211105110652 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sport_attente ADD user_sportif_id INT NOT NULL');
        $this->addSql('ALTER TABLE sport_attente ADD CONSTRAINT FK_B5C3156358D0CED7 FOREIGN KEY (user_sportif_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_B5C3156358D0CED7 ON sport_attente (user_sportif_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sport_attente DROP FOREIGN KEY FK_B5C3156358D0CED7');
        $this->addSql('DROP INDEX IDX_B5C3156358D0CED7 ON sport_attente');
        $this->addSql('ALTER TABLE sport_attente DROP user_sportif_id');
    }
}
