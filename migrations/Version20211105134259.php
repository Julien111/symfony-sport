<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211105134259 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sports ADD user_sportif_id INT NOT NULL');
        $this->addSql('ALTER TABLE sports ADD CONSTRAINT FK_73C9F91C58D0CED7 FOREIGN KEY (user_sportif_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_73C9F91C58D0CED7 ON sports (user_sportif_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sports DROP FOREIGN KEY FK_73C9F91C58D0CED7');
        $this->addSql('DROP INDEX IDX_73C9F91C58D0CED7 ON sports');
        $this->addSql('ALTER TABLE sports DROP user_sportif_id');
    }
}
