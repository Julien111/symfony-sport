<?php

namespace App\Form;

use App\Entity\Sports;
use App\Entity\Pratiques;
use App\Repository\SportsRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PratiquesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('niveau', ChoiceType::class, [
            'label' => 'Choisir votre niveau :',
            'choices'  => [
                    'Débutant' => 'Débutant',
                    'Confirmé' => 'Confirmé',
                    'Professionnel' => 'Professionnel',
                    'Supporter' => 'Supporter',
                ],
            'required' => true])             
            ->add('sportsData', EntityType::class, [                
                'class' => Sports::class,
                'label' => 'Votre sport : ',
                'query_builder' => function (SportsRepository $sportValide) {
                    return $sportValide->createQueryBuilder('sports')                        
                        ->andWhere('sports.is_verified = 1');
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Pratiques::class,
        ]);
    }
}