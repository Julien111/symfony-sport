<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211031213830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratiques ADD sportifpratiques_id INT NOT NULL');
        $this->addSql('ALTER TABLE pratiques ADD CONSTRAINT FK_81B08E0A5B86C27D FOREIGN KEY (sportifpratiques_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_81B08E0A5B86C27D ON pratiques (sportifpratiques_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratiques DROP FOREIGN KEY FK_81B08E0A5B86C27D');
        $this->addSql('DROP INDEX IDX_81B08E0A5B86C27D ON pratiques');
        $this->addSql('ALTER TABLE pratiques DROP sportifpratiques_id');
    }
}
