<?php

namespace App\Controller\Profil;

use App\Entity\Users;
use App\Entity\Sports;
use App\Form\SearchType;
use App\Entity\Pratiques;
use App\Service\Securizer;
use App\Form\PratiquesType;
use App\Form\SportsFormType;
use App\Repository\UsersRepository;
use App\Repository\PratiquesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Proxies\__CG__\App\Entity\Users as EntityUsers;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/profil")
 * @IsGranted("ROLE_SPORTIF")
 */
class ProfilController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    
    /**
     * @Route("/", name="profil_index", methods={"GET"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function index(PratiquesRepository $pratiquesRepository, Securizer $securizer): Response
    {   

        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('profil/index.html.twig', [
            'pratiques' => $pratiquesRepository->findBy(array('sportifpratiques' => $this->getUser())),
        ]);
    }

    /**
     * @Route("/new", name="profil_new", methods={"GET","POST"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function new(Request $request, PratiquesRepository $pratiqueSport, UsersRepository $usersRepository, Securizer $securizer): Response
    {

        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        $pratique = new Pratiques();
        $form = $this->createForm(PratiquesType::class, $pratique);
        $form->handleRequest($request);
        $user = $this->security->getUser();        
        

        if ($form->isSubmitted() && $form->isValid()) {

            //Si l'utilisateur a déjà enregistré le sport il ne peut pas dupliquer une même pratique (niveau)
            
            $uniqueSport = $pratiqueSport->findBy(array('sportsData' => $form->get('sportsData')->getData()->getId(), 'sportifpratiques' => $this->getUser()->getId()));
        
            if($uniqueSport){
                $this->addFlash('warning', 'Vous ne pouvez pas avoir plusieurs niveaux sur un même sport, modifié l\'activité existante.');
                return $this->redirectToRoute('profil_index', [], Response::HTTP_SEE_OTHER);
            }

            $pratique->setSportifpratiques($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pratique);
            $entityManager->flush();

            return $this->redirectToRoute('profil_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('profil/new.html.twig', [
            'pratique' => $pratique,
            'form' => $form,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/new-sport", name="profil_new_sport", methods={"GET","POST"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function newSport(Request $request, Securizer $securizer): Response
    {
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        $sport = new Sports();
        $form = $this->createForm(SportsFormType::class, $sport);
        $form->handleRequest($request);
        $user = $this->security->getUser();        
        

        if ($form->isSubmitted() && $form->isValid()) {            
            $sport->setIsVerified("0");
            $sport->setUserSportif($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sport);
            $entityManager->flush();
            
            $this->addFlash('success', 'Votre sport doit être validé par les administrateurs du site avant d\'être disponible.');
            return $this->redirectToRoute('profil_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('profil/newSport.html.twig', [
            'sport' => $sport,
            'form' => $form,
            'user' => $user,
        ]);
    }

     /** 
     * @Route("/search", name="profilsearch", methods={"GET","POST"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function search(Request $request, PratiquesRepository $pratiquesRepository, Securizer $securizer): Response
    {

        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $niveau = $form->get('niveau')->getData();
            $id_sport = $form->get('sportsData')->getData()->getId();
            $id_depart = $form->get('departements')->getData()->getId();

            if(isset($niveau) && !empty($niveau)){
                $sportifs = $pratiquesRepository->getUsersBypratique($niveau, $id_sport, $id_depart);
            }
            else{
                $sportifs = $pratiquesRepository->getUsersBypratiqueAll($id_sport, $id_depart);                
            }

            
            return $this->searchResult([$sportifs], $securizer);
        }
    
        return $this->renderForm('profil/search.html.twig', [
            'form' => $form,            
        ]);
    }
    /**
     * @Route("/search/result", name="profilsearch_result", methods={"GET","POST"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function searchResult(array $sportifs, Securizer $securizer): Response
    {    
    
        if(empty($sportifs)){
            $this->addFlash('message', 'Aucun résultat trouvé');
            return $this->redirectToRoute('profilsearch');
        }
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }
        
        return $this->render('profil/result.html.twig', ["sportifs" => $sportifs]);
    }

    /**
     * @Route("/{id}", name="profil_show", methods={"GET"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function show(Pratiques $pratique, Securizer $securizer): Response
    {
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('profil/show.html.twig', [
            'pratique' => $pratique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="profil_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function edit(Request $request, Pratiques $pratique, Securizer $securizer): Response
    {
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        $form = $this->createForm(PratiquesType::class, $pratique);
        $form->handleRequest($request);        

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('profil/edit.html.twig', [
            'pratique' => $pratique,
            'form' => $form,            
        ]);
    }

    /**
     * @Route("/{id}", name="profil_delete", methods={"POST"})
     * @IsGranted("ROLE_SPORTIF")
     */
    public function delete(Request $request, Pratiques $pratique, Securizer $securizer): Response
    {
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_index');
        }

        if ($this->isCsrfTokenValid('delete'.$pratique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pratique);
            $entityManager->flush();
        }

        return $this->redirectToRoute('profil_index', [], Response::HTTP_SEE_OTHER);
    }   
}