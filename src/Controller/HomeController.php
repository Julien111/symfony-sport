<?php

namespace App\Controller;

use App\Entity\Sports;
use App\Repository\SportsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(SportsRepository $sportsRepository): Response
    {
        $sports = $sportsRepository->findBy(array('is_verified' => true));     
        
        return $this->render('home/index.html.twig', [
            'sports' => $sports,            
        ]);
    }

    /**
     * @Route("/about", name="app_about")
     */
    public function about(): Response
    {
        $title = "About us";         
        
        return $this->render('about/index.html.twig', [
            'title' => $title,
        ]);
    }
}