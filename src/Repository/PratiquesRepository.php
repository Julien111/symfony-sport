<?php

namespace App\Repository;

use App\Entity\Pratiques;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pratiques|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pratiques|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pratiques[]    findAll()
 * @method Pratiques[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PratiquesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pratiques::class);
    }

    /**
    * @return Pratiques[] Returns an array of Pratiques objects
    */
    
    public function getUsersBypratique($niveau, $id_sport, $id_depart)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.niveau = :niveau')
            ->setParameter('niveau', $niveau)
            ->andWhere('p.sportsData = :val')
            ->setParameter('val', $id_sport)
            ->leftJoin('p.sportifpratiques', 'users')
            ->andWhere('users.departements = :depart')
            ->setParameter('depart', $id_depart)          
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return Pratiques[] Returns an array of Pratiques objects
    */
    
    public function getUsersBypratiqueAll($id_sport, $id_depart)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.sportsData = :val')
            ->setParameter('val', $id_sport)
            ->leftJoin('p.sportifpratiques', 'users')
            ->andWhere('users.departements = :depart')
            ->setParameter('depart', $id_depart)          
            ->getQuery()
            ->getResult()
        ;
    }
    
    /*
    public function findOneBySomeField($value): ?Pratiques
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}